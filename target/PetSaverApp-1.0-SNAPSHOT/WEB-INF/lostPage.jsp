<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Elveszett kisállatok</title>
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Slabo+27px|Yesteryear'>
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'><link rel="stylesheet" href="./style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css">
        <style>
            * {
                box-sizing: border-box;
            }

            body {
                background: #eceef1;
                font-family: 'Slabo 27px', serif;
                color: #333a45;
            }

            .wrapper {
                margin: 5em auto;
                max-width: 1000px;
                background-color: #dff;
                box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.06);
            }

            .header {
                padding: 30px 30px 0;
                text-align: center;
            }
            .header__title {
                margin: 0;
                text-transform: uppercase;
                font-size: 2.5em;
                font-weight: 500;
                line-height: 1.1;
            }
            .header__subtitle {
                margin: 0;
                font-size: 1.5em;
                color: #949fb0;
                font-family: 'Yesteryear', cursive;
                font-weight: 500;
                line-height: 1.1;
            }

            .cards {
                padding: 15px;
                display: flex;
                flex-flow: row wrap;
            }

            .card {
                margin: 15px;
                width: calc((100% / 3) - 30px);
                transition: all 0.2s ease-in-out;
            }
            @media screen and (max-width: 991px) {
                .card {
                    width: calc((100% / 2) - 30px);
                }
            }
            @media screen and (max-width: 767px) {
                .card {
                    width: 100%;
                }
            }
            .card:hover .card__inner {
                background-color: #1abc9c;
                -webkit-transform: scale(1.05);
                transform: scale(1.05);
            }
            .card__inner {
                width: 100%;
                padding: 30px;
                position: relative;
                cursor: pointer;
                background-color: #949fb0;
                color: #eceef1;
                font-size: 1.5em;
                text-transform: uppercase;
                text-align: center;
                transition: all 0.2s ease-in-out;
            }
            .card__inner:after {
                transition: all 0.3s ease-in-out;
            }
            .card__inner .fa {
                width: 100%;
                margin-top: .25em;
            }


            .card.is-expanded:hover .card__inner {
                -webkit-transform: scale(1);
                transform: scale(1);
            }
            .card.is-inactive .card__inner {
                pointer-events: none;
                opacity: 0.5;
            }
            .card.is-inactive:hover .card__inner {
                background-color: #949fb0;
                -webkit-transform: scale(1);
                transform: scale(1);
            }

            @media screen and (min-width: 992px) {
                .card:nth-of-type(3n+2) .card__expander {
                    margin-left: calc(-100% - 30px);
                }

                .card:nth-of-type(3n+3) .card__expander {
                    margin-left: calc(-200% - 60px);
                }

                .card:nth-of-type(3n+4) {
                    clear: left;
                }

                .card__expander {
                    width: calc(300% + 60px);
                }
            }
            @media screen and (min-width: 768px) and (max-width: 991px) {
                .card:nth-of-type(2n+2) .card__expander {
                    margin-left: calc(-100% - 30px);
                }

                .card:nth-of-type(2n+3) {
                    clear: left;
                }

                .card__expander {
                    width: calc(200% + 30px);
                }
            }


            body, html {height: 100%}

            .top-w3-top{
                font-family: "Amatic SC", sans-serif;
            }

            .footer-w3-container{
                margin-bottom: 0;
            }

            .elveszett{
                background-color: #F5F5DC;
                font-family: "Arial",sans-serif;
                font-size: 1rem;

            }

            .jumbotron-fluid{
                background-color: #F5F5DC;
                background-attachment: fixed;
                background-size: cover;
                background-position: center;
                color: black;
                margin-top: 50px;
            }

            .jumbotron-fluid display-4{
                font-family: "Amatic SC", sans-serif;
                font-size: 3rem;
                text-align: center;
            }

            .title {
                color: grey;
                font-size: 15px;
            }

            button {
                border: none;
                outline: 0;
                display: inline-block;
                padding: 8px;
                color: white;
                background-color: #000;
                text-align: center;
                cursor: pointer;
                width: 100%;
                font-size: 18px;
            }

            a {
                text-decoration: none;
                font-size: 22px;
                color: black;
            }

            button:hover, a:hover {
                opacity: 0.7;
            }



            .btn-outline-info{
                border: 2px solid;


            }
            .table-lost {
                background-color: #fff;
                padding: 10px;
                border: 1px solid darken;
                box-shadow: 3px 3px 0 rgba(0,0,0,0.1);
                max-width: calc(100% - 2em);
                margin: 1em auto;
                overflow: hidden;
                width: 800px;
                border: none;
                box-shadow: none;
                overflow: visible;
            }

            table {
                width: 100%;

                td, th {
                    color: rgba(217, 136, 89, 1);
                }

                td {
                    text-align: center;
                    vertical-align: middle;

                    &:last-child {
                        font-size: 0.95em;
                        line-height: 1.4;
                        text-align: left;
                    }
                }

                th {
                    background-color: lighten;
                    font-weight: 300;
                }

                tr {
                    &:nth-child(2n) { background-color: white; }
                    &:nth-child(2n+1) { background-color: lighten }
                }
            }

            @media screen and (max-width: 700px) {
                table, tr, td { display: block; }

                td {
                    &:first-child {
                        position: absolute;
                        top: 50%;
                        transform: translateY(-50%);
                    }

                    &:not(:first-child) {
                        clear: both;
                        padding: 4px 20px 4px 90px;
                        position: relative;
                        text-align: left;

                        &:before {
                            color: lighten;
                            display: block;
                            left: 0;
                            position: absolute;
                        }
                    }

                    &:nth-child(2):before { content: 'Kép:'; }
                    &:nth-child(3):before { content: 'Neve:'; }
                    &:nth-child(4):before { content: 'Neme:'; }
                    &:nth-child(4):before { content: 'Város:'; }
                    &:nth-child(5):before { content: 'Elveszett:'; }
                }

                tr {
                    position: relative;
                    &:first-child { display: none; }
                }
            }

        </style>
        <script>
            var $cell = $('.card');

            //open and close card when clicked on card
            $cell.find('.js-expander').click(function () {

                var $thisCell = $(this).closest('.card');

                if ($thisCell.hasClass('is-collapsed')) {
                    $cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed').addClass('is-inactive');
                    $thisCell.removeClass('is-collapsed').addClass('is-expanded');

                    if ($cell.not($thisCell).hasClass('is-inactive')) {
                        //do nothing
                    } else {
                        $cell.not($thisCell).addClass('is-inactive');
                    }

                } else {
                    $thisCell.removeClass('is-expanded').addClass('is-collapsed');
                    $cell.not($thisCell).removeClass('is-inactive');
                }
            });

            //close card when click on cross
            $cell.find('.js-collapser').click(function () {

                var $thisCell = $(this).closest('.card');

                $thisCell.removeClass('is-expanded').addClass('is-collapsed');
                $cell.not($thisCell).removeClass('is-inactive');

            });

            $(document).ready(function () {
                $('img').on('click', function () {
                    var image = $(this).attr('src');
                    //alert(image);
                    $('#myModal').on('show.bs.modal', function () {
                        $(".showimage").attr("src", image);
                    });
                });
            });



        </script>
    </head>
    <body class="elveszett">
        <!-- Navbar (sit on top) -->
        <div class="w3-top top-w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>

                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>

                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <h1 style="font-size:2vw; color:black; text-align:center">Elveszett kisállatok:</h1>
        <div class="wrapper">
            <div class="header">
                <h1 class="header__title"></h1>
                <h2 class="header__subtitle"></h2>
            </div>
            <c:if test="${not empty lostPetsList}">
                <div class="cards">                 
                    <c:forEach items="${lostPetsList}" var="item">
                        <c:url var="updateLink" value="lostPetEditServlet">
                            <c:param name="petId" value="${item.id}"/>
                            <c:param name="catOrDog" value="${item.catOrDog}"/>
                            <c:param name="type" value="${item.type}"/>
                            <c:param name="sex" value="${item.sex}"/>
                            <c:param name="age" value="${item.age}"/>
                            <c:param name="color" value="${item.color}"/>
                            <c:param name="weight" value="${item.weight}"/>
                            <c:param name="image" value="${item.image}"/>
                        </c:url>
                        <div class=" card [ is-collapsed ] ">
                            <div class="card__inner [ js-expander ]" style="width: 20rem; text-align:center;display:inline-block;">
                                <img class="card-img-top" src="DisplayImageServlet?name=${item.image}" alt="lost pet" height="200" width="200" data-toggle="modal" data-target="#myModal" style="border-radius:.60rem; object-fit:cover">
                                <div class="card-block">
                                    <h4 class="card-title">Név: "${item.nickName}"</h4>
                                    <p class="card-text">Kora: ${item.age}</p>
                                    <td><input type="hidden" id="petId" name="petId" value="${item.id}"></td>
                                    <a href="lostPetAdditionalInfoServlet?petId=${item.id}" class="btn btn-primary">Bövebb info</a>
                                    <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                                        <a href="lostPetEditServlet?petId=${item.id}" class="btn btn-primary">Szerkesztés</a>
                                    </c:if>
                                </div>
                            </div>
                        </div>

                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <img class="showimage img-responsive" src="" />
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>                 
                </div>
            </c:if>
        </div>
        <c:if test="${empty lostPetsList}">
            <h3>Jelenleg nincs elveszett kisállat</h3>
        </c:if>              
        <!-- Footer -->
        <footer class="w3-container footer-w3-container w3-padding-64 w3-buttom w3-opacity w3-light-grey w3-xlarge">
            <div class="footer-copyright text-center py-3">© 2019 Copyright:
                <a href="https://wwf.hu/" style="color:#009900"> PetSaver</a>
            </div>
        </footer>
    </body>
</html>