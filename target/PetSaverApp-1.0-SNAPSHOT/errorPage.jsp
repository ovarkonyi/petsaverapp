<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Error</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <!--<link href="/resources/PetSaverApp_sample.css" rel="stylesheet" type="text/css">-->
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/style/PetSaverApp_sample.css" />" >
    </head>
    <body>

    <div class="container-fluid text-center">    
        <div class="row content">         
            <div style="background-color: rgba(0,0,0,0.6)" class="col-sm-8 text-left"> 
                                              
                
                    <section class="color-1">
                        <h1 style="font-size:2vw; color:white">Hiba!</h1>  
                    </section>

            </div>
        </div>
    </div>
</body>
</html>
