package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dao.UserDAO;
import hu.bh10.petsaverapp.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/loginServlet"})
@DeclareRoles({"admin", "user"})
public class LoginServlet extends HttpServlet {

    @Inject
    UserService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.getRequestDispatcher("loginPage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String email = request.getParameter("email");
//        String password = request.getParameter("password");
//        HttpSession session = request.getSession();
//        PrintWriter out = response.getWriter();
//
//        RequestDispatcher rd = request.getRequestDispatcher("homePageServlet");
//        try {
//            boolean authenticate = service.authenticateUser(email, password);
//            if(authenticate) {
//                    session.setAttribute("email", email);
//                    rd.forward(request, response);				
//            }else{
//                out.println("Nem tudjuk authentikálni.");
//            }
//        } catch (Exception e) {
//                out.println("A kérés nem végezhető el, próbálja később.");
//        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
