package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.service.PetService;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(urlPatterns = {"/lostPetAdditionalInfoServlet"})
public class LostPetAdditionalInfoServlet extends HttpServlet {
    
    @Inject
    private PetService petService;
    
   

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //El kell kérni a foundPage.jsp-től a petId-t
        HttpSession session = request.getSession();
        Long petId = Long.valueOf(request.getParameter("petId"));
        System.out.println("Kiallat ID: "+petId);
        PetDTO pet = PetMapper.toPetDTO(petService.getPetById(petId));
        session.setAttribute("pet", pet);
        request.getRequestDispatcher("WEB-INF/lostPetAdditionalInfoPage.jsp").forward(request, response);
         
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          request.setCharacterEncoding("UTF-8");
        
   
      
        response.sendRedirect("lostPetAdditionalInfoServlet");
    }

}
