package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.dto.LostDTO;
import hu.bh10.petsaverapp.service.AnnouncerService;
import hu.bh10.petsaverapp.service.FoundFormService;
import hu.bh10.petsaverapp.service.LostFormService;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "updatePetServlet", urlPatterns = {"/updatePetServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class UpdatePetServlet extends HttpServlet {

    @Inject
    private FoundFormService foundFormService;
    
    @Inject
    private LostFormService lostFormService;
    
    @Inject
    private AnnouncerService announcerService;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        RequestDispatcher dispatcher = null;
        //Kell egy elágazás, hogy Lost gyorsbejelentőről vagy Found gyorsbejelentőről jött -e a bejelentés
            
        if(request.getParameter("nickName") == null){//Ha nincs neve a kisállatnak akkor FOUND
            
            Long id = Long.valueOf(request.getParameter("id"));
            String foundPlace = request.getParameter("foundPlace");
            String sex = request.getParameter("sex");
            String petProperties = request.getParameter("petProperties");

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate foundDate = LocalDate.parse(request.getParameter("foundDate"), dtf);

            String image = request.getParameter("image");
            String announcerEmail = request.getParameter("announcer");
            boolean added = false;//Kezdetben még biztos nincs szerkesztve és hozzáadva a Megtalált állatok listázásához.
            AnnouncerDTO announcer = announcerService.getAnnouncerByEmail(announcerEmail);

            FoundDTO pet = new FoundDTO(foundPlace, sex, petProperties, foundDate, image, announcer, added); //announcer??
            request.setAttribute("pet", pet);
            request.setAttribute("foundId", id);
            dispatcher = request.getRequestDispatcher("WEB-INF/updateFoundPetPage.jsp");
            
        }else{
            
            Long id = Long.valueOf(request.getParameter("id"));
            String nickName = request.getParameter("nickName");
            String lastKnownPlace = request.getParameter("lastKnownPlace");
            String sex = request.getParameter("sex");
            String petProperties = request.getParameter("petProperties");

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate lostDate = LocalDate.parse(request.getParameter("lostDate"), dtf);

            String image = request.getParameter("image");
            String announcerEmail = request.getParameter("announcer");
            boolean added = false;//Kezdetben még biztos nincs szerkesztve és hozzáadva az Elveszett állatok listázásához.
            AnnouncerDTO announcer = announcerService.getAnnouncerByEmail(announcerEmail);

            LostDTO pet = new LostDTO(nickName, lastKnownPlace, sex, petProperties, lostDate, image, announcer, added); //announcer??
            request.setAttribute("pet", pet);
            request.setAttribute("lostId", id);
            dispatcher = request.getRequestDispatcher("WEB-INF/updateLostPetPage.jsp");
            
        }
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
