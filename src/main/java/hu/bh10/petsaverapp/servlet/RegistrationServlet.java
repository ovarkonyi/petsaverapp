package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dao.UserDAO;
import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/registrationServlet"})
@DeclareRoles({"admin", "user"})
public class RegistrationServlet extends HttpServlet {
    
    @Inject
    UserService service;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registrationPage.jsp");
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String nickName = request.getParameter("nickName");
        String email = request.getParameter("email");
        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");

        RequestDispatcher rd1 = request.getRequestDispatcher("/WEB-INF/registrationPage.jsp");
        RequestDispatcher rd2 = request.getRequestDispatcher("WEB-INF/newHomePage.jsp");
        RequestDispatcher rd3 = request.getRequestDispatcher("./errorPage.jsp");
        PrintWriter out=response.getWriter();

        UserDTO user = new UserDTO();
        boolean isExist = false;
        try {
            isExist = service.checkUserIfExist(email);
        } catch (Exception ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(isExist) {
            out.println("Önkéntes "+email+" email cimmel már létezik.");
            rd3.include(request, response);
        }else {
            if(!password.equals(confirmPassword)) {
                // logger log4j, logback
                // logger.log()
                    out.println("A két jelszó nem egyezik.");
                    rd1.forward(request, response);
            }else {
                    String role = "user";
                    boolean valid = false;

                    user.setNickName(nickName);
                    user.setRole(role);
                    user.setUserEmail(email);
                    user.setUserFirstName(firstName);
                    user.setUserLastName(lastName);
                    user.setUserPassword(password);
                    user.setValid(valid);
                    service.registerUser(user);

                    //out.println("Sikeres regisztráció!");
                    //rd2.include(request, response);
                    rd2.forward(request, response);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
