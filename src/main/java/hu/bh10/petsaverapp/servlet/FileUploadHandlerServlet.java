package hu.bh10.petsaverapp.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet(urlPatterns = {"/fileUploadHandlerServlet"})
public class FileUploadHandlerServlet extends HttpServlet {

    private static final String UPLOAD_DIRECTORY = "..\\uploaded_pet_images\\";
    private static final Integer MAX_FILE_SIZE_IN_BYTES = 1024 * 1024 * 10;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processCommonLogic(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processCommonLogic(request, response);
    }

    private void processCommonLogic(HttpServletRequest request, HttpServletResponse response) {
        String file_name = null;
        response.setContentType("text/html; charset=UTF-8");
        boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
        if (!isMultipartContent) {
            return;
        }

        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        String uploadPath = getServletContext().getRealPath("")
                + File.separator + UPLOAD_DIRECTORY;
        try {
            List<FileItem> fields = upload.parseRequest(request);
            Iterator<FileItem> it = fields.iterator();
            if (!it.hasNext()) {
                return;
            }
            while (it.hasNext()) {
                FileItem fileItem = it.next();
                boolean isFormField = fileItem.isFormField();
                if (isFormField) {
                    if (file_name == null) {
                        if (fileItem.getFieldName().equals("file_name")) {
                            file_name = fileItem.getString();
                        }
                    }
                } else { // konstansok
                    if (fileItem.getSize() < MAX_FILE_SIZE_IN_BYTES && (fileItem.getContentType().equals("image/jpeg") || fileItem.getContentType().equals("image/jpg") || fileItem.getContentType().equals("image/png") || fileItem.getContentType().equals("image/bmp"))) {
                        String imageSourcePath = uploadPath + fileItem.getName();

                        File uploadDir = new File(uploadPath);
                        if (!uploadDir.exists()) {
                            uploadDir.mkdir();
                        }

                        fileItem.write(new File(imageSourcePath));
                        HttpSession session = request.getSession();
                        String url = request.getHeader("referer");
                        session.setAttribute("url", url);
                        request.setAttribute("imageLink", imageSourcePath);
                        request.getRequestDispatcher("WEB-INF/newHomePage.jsp").include(request, response);
//                        request.getRequestDispatcher(url).include(request, response);
                    } else {
                        request.getRequestDispatcher("WEB-INF/imageUploadErrorPage.jsp").include(request, response);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
