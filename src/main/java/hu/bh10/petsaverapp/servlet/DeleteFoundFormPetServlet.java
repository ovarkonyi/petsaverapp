package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.service.FoundFormService;
import java.io.IOException;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DeleteFoundFormPetServlet", urlPatterns = {"/deleteFoundFormPetServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class DeleteFoundFormPetServlet extends HttpServlet {
    
    @Inject
    private FoundFormService foundFormService;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Long foundId = Long.valueOf(request.getParameter("foundId"));
        foundFormService.removeFound(foundId);
        response.sendRedirect("userPageServlet");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("userPageServlet");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
