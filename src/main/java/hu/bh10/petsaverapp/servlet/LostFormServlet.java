package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.dto.LostDTO;
import hu.bh10.petsaverapp.service.AnnouncerService;
import hu.bh10.petsaverapp.service.EmailSend;
import hu.bh10.petsaverapp.service.LostFormService;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/lostFormServlet"})
@DeclareRoles({"admin", "user"})
public class LostFormServlet extends HttpServlet {

    @Inject
    private LostFormService lostFormService;

    @Inject
    private AnnouncerService announcerService;

    @Inject
    private EmailSend emailSend;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/homePage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        // refaktor

        String nickName = (String) request.getParameter("nickName");
        String lastKnownPlace = (String) request.getParameter("lastKnownPlace");
        String sex = request.getParameter("sex");
        String petProperties = (String) request.getParameter("petProperties");
        //String to LocalDate
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate lostDate = LocalDate.parse(request.getParameter("lostDate"), dtf);

        String image = (String) request.getParameter("image");
        String announcerName = (String) request.getParameter("lostName");
        String announcerPhone = (String) request.getParameter("lostPhone");
        String announcerMail = (String) request.getParameter("lostMail");

        AnnouncerDTO freshAnnouncer = new AnnouncerDTO();
        freshAnnouncer.setNameOfAnnouncer(announcerName);
        freshAnnouncer.setPhoneOfAnnouncer(announcerPhone);
        freshAnnouncer.setEmailOfAnnouncer(announcerMail);
        announcerService.createAnnouncer(freshAnnouncer);

        AnnouncerDTO savedAnnouncer = announcerService.getAnnouncerByEmail(announcerMail);

        LostDTO lost = new LostDTO();
        lost.setNickName(nickName);
        lost.setLastKnownPlace(lastKnownPlace);
        lost.setSex(sex);
        lost.setPetProperties(petProperties);
        lost.setLostDate(lostDate);
        lost.setImage(image);
        lost.setAdded(false);
        lost.setAnnouncer(savedAnnouncer);
        Long savedAnnouncerId = Long.valueOf(savedAnnouncer.getAnnouncerId());

        lostFormService.createLost(lost, savedAnnouncerId);

//        response.sendRedirect("homePageServlet");
        emailSend.sendingEmail(announcerName, announcerMail);
        request.getRequestDispatcher("WEB-INF/successfullNotification.jsp").include(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
