package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.ChipEntity.NQ_GET_CHIPS_BY_ID;
import static hu.bh10.petsaverapp.entity.ChipEntity.NQ_GET_CHIP_BY_ID;
import static hu.bh10.petsaverapp.entity.ChipEntity.PARAM_CHIP_ID;
import static hu.bh10.petsaverapp.entity.ChipEntity.PARAM_PET_ID;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "chip")
@NamedQueries({
    @NamedQuery(name = NQ_GET_CHIPS_BY_ID, query = "select c from ChipEntity c where c.chipedPet.id =:"+ PARAM_PET_ID),
    @NamedQuery(name = NQ_GET_CHIP_BY_ID, query = "select c from ChipEntity c where c.id =:"+ PARAM_CHIP_ID)
})
public class ChipEntity extends BaseEntity {
    
    public static final String NQ_GET_CHIPS_BY_ID = "ChipEntity.getChips";
    public static final String PARAM_PET_ID = "petId";
    public static final String NQ_GET_CHIP_BY_ID = "ChipEntity.getChip";
    public static final String PARAM_CHIP_ID = "chipId";

    @Column(name = "chip_number", unique = true)
    private String chipNumber;
    
    @Column(name = "chip_date")
    private Date chipDate;

    @Column(name = "vet_name")
    private String vetName;

    @JoinColumn(name = "pet_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private PetEntity chipedPet;

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public Date getChipDate() {
        return chipDate;
    }

    public void setChipDate(Date chipDate) {
        this.chipDate = chipDate;
    }

    public String getVetName() {
        return vetName;
    }

    public void setVetName(String vetName) {
        this.vetName = vetName;
    }

    public PetEntity getChipedPet() {
        return chipedPet;
    }

    public void setChipedPet(PetEntity chipedPet) {
        this.chipedPet = chipedPet;
    }

    

}
