package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.ChipDAO;
import hu.bh10.petsaverapp.dao.PetDAO;
import hu.bh10.petsaverapp.dto.ChipDTO;
import hu.bh10.petsaverapp.entity.ChipEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.mapper.ChipMapper;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class ChipService {
    
    @Inject
    ChipDAO chipDAO;
    
    @Inject
    PetDAO petDAO;
    
    public void createChip(ChipDTO chip, Long petId) {
        PetEntity petEntity = petDAO.getPetById(petId);
        ChipEntity entity = ChipMapper.toEntity(chip, petEntity);
        chipDAO.createEntity(entity);
    }

    public ChipService() {
    }

    public ChipService(ChipDAO chipDAO, PetDAO petDAO) {
        this.chipDAO = chipDAO;
        this.petDAO = petDAO;
    }
    
    public List<ChipDTO> getChipsById(Long petId){
        List<ChipDTO> chips = new ArrayList<>();
        chips = ChipMapper.toChipDTOList(chipDAO.getChipsById(petId));
        return chips;
    }
    
    public void updateChip(String chipDateString, String chipNumber, String vetNameChip, Long petId, Long chipId) {
        if((chipDateString!=null && !chipDateString.isEmpty()) && (chipNumber!=null && !chipNumber.isEmpty())){
            ChipEntity entity = chipDAO.getChipById(chipId);
            Date chipDate = Date.valueOf(chipDateString);
            entity.setChipDate(chipDate);
            entity.setChipNumber(chipNumber);
            entity.setVetName(vetNameChip);
            chipDAO.updateEntity(entity);
        }
        
    }
}
