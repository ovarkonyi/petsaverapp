package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.HealthDiagnoseDAO;
import hu.bh10.petsaverapp.dao.PetDAO;
import hu.bh10.petsaverapp.dto.HealthDiagnoseDTO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.entity.HealthDiagnoseEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.mapper.HealthDiagnoseMapper;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class HealthDiagnoseService {
    
    @Inject
    HealthDiagnoseDAO healthDiagnoseDAO;
    
    @Inject
    PetDAO petDAO;
    
    public void createHealthDiagnose(HealthDiagnoseDTO healthDiagnose, Long petId) {
        PetEntity petEntity = petDAO.getPetById(petId);
            HealthDiagnoseEntity entity = HealthDiagnoseMapper.toEntity(healthDiagnose, petEntity);
            healthDiagnoseDAO.createEntity(entity);
    }
    
    public void createHealthDiagnose(String diagDateString, String cause, String result, String medicine, String vetNameDiag, Long petId) {
        if(diagDateString!=null && !diagDateString.isEmpty()){
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate diagDate = LocalDate.parse(diagDateString, dtf);
            HealthDiagnoseDTO healthDiagnoseDTO = new HealthDiagnoseDTO();
            healthDiagnoseDTO.setDiagDate(diagDate);
            healthDiagnoseDTO.setCause(cause);
            healthDiagnoseDTO.setResult(result);
            healthDiagnoseDTO.setMedicine(medicine);
            healthDiagnoseDTO.setVetName(vetNameDiag);
            PetEntity petEntity = petDAO.getPetById(petId);
            HealthDiagnoseEntity entity = HealthDiagnoseMapper.toEntity(healthDiagnoseDTO , petEntity);
            healthDiagnoseDAO.createEntity(entity);
        }
        
    }
    
    public List<HealthDiagnoseDTO> getDiagnosesById(Long petId){
        List<HealthDiagnoseDTO> diagnoses = new ArrayList<>();
        diagnoses = HealthDiagnoseMapper.toDiagnoseDTOList(healthDiagnoseDAO.getDiagnosesById(petId));
        return diagnoses;
    }
}
