package hu.bh10.petsaverapp.dto;

import java.time.LocalDate;

public class LostDTO {
    
    private Long id;
    private String nickName;
    private String lastKnownPlace;
    private String sex;
    private String petProperties;
    private LocalDate lostDate;
    private String image;
    private AnnouncerDTO announcer;
    private boolean added;

    public LostDTO() {
    }

 
    public LostDTO(String nickName, String lastKnownPlace, String sex, String petProperties, LocalDate lostDate, String image, AnnouncerDTO announcer, boolean added) {
        this.nickName = nickName;
        this.lastKnownPlace = lastKnownPlace;
        this.sex = sex;
        this.petProperties = petProperties;
        this.lostDate = lostDate;
        this.image = image;
        this.announcer = announcer;
        this.added = added;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getLastKnownPlace() {
        return lastKnownPlace;
    }

    public void setLastKnownPlace(String lastKnownPlace) {
        this.lastKnownPlace = lastKnownPlace;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPetProperties() {
        return petProperties;
    }

    public void setPetProperties(String petProperties) {
        this.petProperties = petProperties;
    }

    public LocalDate getLostDate() {
        return lostDate;
    }

    public void setLostDate(LocalDate lostDate) {
        this.lostDate = lostDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public AnnouncerDTO getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(AnnouncerDTO announcer) {
        this.announcer = announcer;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    @Override
    public String toString() {
        return "LostDTO{" + "nickName=" + nickName + ", lastKnownPlace=" + lastKnownPlace + ", sex=" + sex + ", petProperties=" + petProperties + ", lostDate=" + lostDate + ", image=" + image + ", announcer=" + announcer + ", added=" + added + '}';
    }

    

}
