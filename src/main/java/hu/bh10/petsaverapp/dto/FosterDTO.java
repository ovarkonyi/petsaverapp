package hu.bh10.petsaverapp.dto;

import java.sql.Date;
import java.time.LocalDate;

public class FosterDTO {
    
    private Long id;
    private String fosterFirstName;  
    private String fosterLastName;  
    private LocalDate fosterDate;  
    private String fosterNote;  
    private String fosterEmail;   
    private String fosterPhone;

    public FosterDTO() {
    }
//
//    public FosterDTO(Long id, String fosterFirstName, String fosterLastName, LocalDate fosterDate, String fosterNote, String fosterEmail, String fosterPhone) {
//        this.id = id;
//        this.fosterFirstName = fosterFirstName;
//        this.fosterLastName = fosterLastName;
//        this.fosterDate = fosterDate;
//        this.fosterNote = fosterNote;
//        this.fosterEmail = fosterEmail;
//        this.fosterPhone = fosterPhone;
//    }

    public FosterDTO(Long id, String fosterFirstName, String fosterLastName, String fosterNote, String fosterEmail, String fosterPhone) {
        this.id = id;
        this.fosterFirstName = fosterFirstName;
        this.fosterLastName = fosterLastName;
        this.fosterNote = fosterNote;
        this.fosterEmail = fosterEmail;
        this.fosterPhone = fosterPhone;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getFosterFirstName() {
        return fosterFirstName;
    }

    public void setFosterFirstName(String fosterFirstName) {
        this.fosterFirstName = fosterFirstName;
    }

    public String getFosterLastName() {
        return fosterLastName;
    }

    public void setFosterLastName(String fosterLastName) {
        this.fosterLastName = fosterLastName;
    }

    public LocalDate getFosterDate() {
        return fosterDate;
    }

    public void setFosterDate(LocalDate fosterDate) {
        this.fosterDate = fosterDate;
    }

    public String getFosterNote() {
        return fosterNote;
    }

    public void setFosterNote(String fosterNote) {
        this.fosterNote = fosterNote;
    }

    public String getFosterEmail() {
        return fosterEmail;
    }

    public void setFosterEmail(String fosterEmail) {
        this.fosterEmail = fosterEmail;
    }

    public String getFosterPhone() {
        return fosterPhone;
    }

    public void setFosterPhone(String fosterPhone) {
        this.fosterPhone = fosterPhone;
    }

    @Override
    public String toString() {
        return "FosterDTO{" + "fosterFirstName=" + fosterFirstName + ", fosterLastName=" + fosterLastName + ", fosterDate=" + fosterDate + ", fosterNote=" + fosterNote + ", fosterEmail=" + fosterEmail + ", fosterPhone=" + fosterPhone + '}';
    }
    
    
}
