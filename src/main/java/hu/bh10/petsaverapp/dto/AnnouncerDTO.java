package hu.bh10.petsaverapp.dto;

public class AnnouncerDTO {
    
    private Long announcerId;
    private String nameOfAnnouncer;    
    private String phoneOfAnnouncer;   
    private String emailOfAnnouncer;

    public AnnouncerDTO() {
    }

    public AnnouncerDTO(Long announcerId, String nameOfAnnouncer, String phoneOfAnnouncer, String emailOfAnnouncer) {
        this.announcerId = announcerId;
        this.nameOfAnnouncer = nameOfAnnouncer;
        this.phoneOfAnnouncer = phoneOfAnnouncer;
        this.emailOfAnnouncer = emailOfAnnouncer;
    }
    
    public long getAnnouncerId() {
        return announcerId;
    }

    public void setAnnouncerId(long announcerId) {
        this.announcerId = announcerId;
    }

    public String getNameOfAnnouncer() {
        return nameOfAnnouncer;
    }

    public void setNameOfAnnouncer(String nameOfAnnouncer) {
        this.nameOfAnnouncer = nameOfAnnouncer;
    }

    public String getPhoneOfAnnouncer() {
        return phoneOfAnnouncer;
    }

    public void setPhoneOfAnnouncer(String phoneOfAnnouncer) {
        this.phoneOfAnnouncer = phoneOfAnnouncer;
    }

    public String getEmailOfAnnouncer() {
        return emailOfAnnouncer;
    }

    public void setEmailOfAnnouncer(String emailOfAnnouncer) {
        this.emailOfAnnouncer = emailOfAnnouncer;
    }

    @Override
    public String toString() {
        return "AnnouncerDTO{" + "announcerId=" + announcerId + ", nameOfAnnouncer=" + nameOfAnnouncer + ", phoneOfAnnouncer=" + phoneOfAnnouncer + ", emailOfAnnouncer=" + emailOfAnnouncer + '}';
    }
  
}
