package hu.bh10.petsaverapp.dto;

import java.time.LocalDate;
import java.time.LocalTime;

public class RescueCircumstanceDTO {
    
    private Long id;
    private LocalDate rescueDate;   
    private LocalTime rescueTime;   
    private String rescuePlace;   
    private String rescueNote;
    private PetDTO pet;

    public RescueCircumstanceDTO() {
    }

    public RescueCircumstanceDTO(Long id, LocalDate rescueDate, LocalTime rescueTime, String rescuePlace, String rescueNote, PetDTO pet) {
        this.id = id;
        this.rescueDate = rescueDate;
        this.rescueTime = rescueTime;
        this.rescuePlace = rescuePlace;
        this.rescueNote = rescueNote;
        this.pet = pet;
    }

    public RescueCircumstanceDTO(LocalDate rescueDate, String rescuePlace, PetDTO pet) {
        this.id = id;
        this.rescueDate = rescueDate;
        this.rescuePlace = rescuePlace;
        this.pet = pet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public LocalDate getRescueDate() {
        return rescueDate;
    }

    public void setRescueDate(LocalDate rescueDate) {
        this.rescueDate = rescueDate;
    }

    public LocalTime getRescueTime() {
        return rescueTime;
    }

    public void setRescueTime(LocalTime rescueTime) {
        this.rescueTime = rescueTime;
    }

    public String getRescuePlace() {
        return rescuePlace;
    }

    public void setRescuePlace(String rescuePlace) {
        this.rescuePlace = rescuePlace;
    }

    public String getRescueNote() {
        return rescueNote;
    }

    public void setRescueNote(String rescueNote) {
        this.rescueNote = rescueNote;
    }

    public PetDTO getPet() {
        return pet;
    }

    public void setPet(PetDTO pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "RescueCircumstanceDTO{" + "rescueDate=" + rescueDate + ", rescueTime=" + rescueTime + ", rescuePlace=" + rescuePlace + ", rescueNote=" + rescueNote + ", pet=" + pet + '}';
    }

    
}
