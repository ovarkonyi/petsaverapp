package hu.bh10.petsaverapp.dto;

import java.time.LocalDate;

public class VaccinationDTO {
    
    public static final String PARVO = "parvo";
    public static final String COMBINATED = "combinated";
    public static final String PARAINFLUENZA = "parainfluenza";
    public static final String BORDATELLA = "bordatella";
    public static final String LEPTOSPIRA = "leptospira";
    public static final String CALICI = "calici";
    public static final String HERPES = "herpes";
    public static final String CHLAMYDIA = "chlamydia";
    
    private Long id;
    private LocalDate vaccDate;    
    private String vaccType;
    private PetDTO pet;

    public VaccinationDTO() {
    }

    public VaccinationDTO(Long id, LocalDate vaccDate, String vaccType, PetDTO pet) {
        this.id = id;
        this.vaccDate = vaccDate;
        this.vaccType = vaccType;
        this.pet = pet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public LocalDate getVaccDate() {
        return vaccDate;
    }

    public void setVaccDate(LocalDate vaccDate) {
        this.vaccDate = vaccDate;
    }

    public String getVaccType() {
        return vaccType;
    }

    public void setVaccType(String vaccType) {
        this.vaccType = vaccType;
    }

    public PetDTO getPet() {
        return pet;
    }

    public void setPet(PetDTO pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "VaccinactionDTO{" + "vaccDate=" + vaccDate + ", vaccType=" + vaccType + ", pet=" + pet + '}';
    }

    
    
}
