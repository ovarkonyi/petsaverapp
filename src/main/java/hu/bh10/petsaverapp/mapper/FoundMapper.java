package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.entity.FoundEntity;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import java.util.List;


public class FoundMapper {

    public FoundMapper() {
    }
    
//    public static FoundEntity toFoundEntity(FoundBean foundBean, AnnouncerEntity announcerEntity) {
//        FoundEntity foundEntity = new FoundEntity();
//        
//        foundEntity.setFoundPlace(foundBean.getFoundPlace());
//        foundEntity.setSex(foundBean.getSex());
//        foundEntity.setPetProperties(foundBean.getPetProperties());
//        LocalDate ld = foundBean.getFoundDate();
//        Date foundDate = Date.valueOf(ld);       
//        foundEntity.setFoundDate(foundDate);
//        foundEntity.setFoundAnnouncer(announcerEntity);
//        foundEntity.setImage(foundBean.getImage());
//        
//        return foundEntity;
//    }
    
    public static FoundEntity toFoundEntity(FoundDTO found, AnnouncerEntity entity) {
        FoundEntity foundEntity = new FoundEntity();
        
        foundEntity.setFoundPlace(found.getFoundPlace());
        foundEntity.setSex(found.getSex());
        foundEntity.setPetProperties(found.getPetProperties());
        LocalDate announceDate = found.getFoundDate();
        Date foundDate = Date.valueOf(announceDate);       
        foundEntity.setFoundDate(foundDate);
        foundEntity.setFoundAnnouncer(entity);
        foundEntity.setImage(found.getImage());
        if(found.isAdded()){
           foundEntity.setAdded("true"); 
        }else{
           foundEntity.setAdded("false");
        }
        
        
        return foundEntity;
    }

    public static FoundDTO toFoundDTO(FoundEntity foundEntity) {

        FoundDTO dto = new FoundDTO();
        dto.setId(foundEntity.getId());
        dto.setFoundPlace(foundEntity.getFoundPlace());
        dto.setSex(foundEntity.getSex());
        dto.setPetProperties(foundEntity.getPetProperties());
        //LocalDate localDate = Date.valueOf("2019-01-10").toLocalDate();
        LocalDate gotDate = foundEntity.getFoundDate().toLocalDate();
        dto.setFoundDate(gotDate);
        dto.setImage(foundEntity.getImage());
        if(foundEntity.getAdded().equals("true")){
            dto.setAdded(true);
        }else{
            dto.setAdded(false);
        }
        //AnnouncerEntity-t át kell mappelni AnnouncerDTO-ra
        dto.setAnnouncer(AnnouncerMapper.toAnnouncerDTO(foundEntity.getFoundAnnouncer()));

        return dto;
    }
    
    
    public static List<FoundDTO> toFoundDTO(List<FoundEntity> foundEntities){
        List<FoundDTO> foundDTOList = new ArrayList<>();
//        foundEntities.forEach(found -> foundDTOList.add(toFoundDTO(found)));
        for (int i = 0; i < foundEntities.size(); i++) {
            FoundDTO foundDTO = new FoundDTO();
            foundDTO.setFoundPlace(foundEntities.get(i).getFoundPlace());
            foundDTO.setSex(foundEntities.get(i).getSex());
            foundDTO.setPetProperties(foundEntities.get(i).getPetProperties());
            LocalDate gotDate = foundEntities.get(i).getFoundDate().toLocalDate();
            foundDTO.setFoundDate(gotDate);
            foundDTO.setImage(foundEntities.get(i).getImage());
            foundDTOList.add(foundDTO);
        }
        return foundDTOList;
             
    }
}
