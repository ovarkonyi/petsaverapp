package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.HealthDiagnoseDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.entity.HealthDiagnoseEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HealthDiagnoseMapper {

    public HealthDiagnoseMapper() {
    }
    
    public static HealthDiagnoseEntity toEntity(HealthDiagnoseDTO dto, PetEntity pet) {
        HealthDiagnoseEntity entity = new HealthDiagnoseEntity();

        Date diagDate = Date.valueOf(dto.getDiagDate());
        entity.setDiagDate(diagDate);
        entity.setCause(dto.getCause());
        entity.setResult(dto.getResult());
        entity.setMedicine(dto.getMedicine());
        entity.setVetName(dto.getVetName());
        entity.setDiagnosedPet(pet);

        return entity;
    }

    public static HealthDiagnoseDTO toDTO(HealthDiagnoseEntity entity, PetDTO petDTO) {

        HealthDiagnoseDTO dto = new HealthDiagnoseDTO();
        
        LocalDate dateFromEntity = entity.getDiagDate().toLocalDate();
        dto.setDiagDate(dateFromEntity);
        dto.setCause(entity.getCause());
        dto.setResult(entity.getResult());
        dto.setMedicine(entity.getMedicine());
        dto.setVetName(entity.getVetName());
        dto.setPet(petDTO);

        return dto;
    }
    
    public static HealthDiagnoseEntity toDiagnoseEntity(HealthDiagnoseDTO dto) {
        HealthDiagnoseEntity entity = new HealthDiagnoseEntity();

        Date diagDate = Date.valueOf(dto.getDiagDate());
        entity.setDiagDate(diagDate);
        entity.setCause(dto.getCause());
        entity.setResult(dto.getResult());
        entity.setMedicine(dto.getMedicine());
        entity.setVetName(dto.getVetName());
        entity.setDiagnosedPet(PetMapper.toPetEntity(dto.getPet()));

        return entity;
    }
    
    public static HealthDiagnoseDTO toDiagnoseDTO(HealthDiagnoseEntity entity) {

        HealthDiagnoseDTO dto = new HealthDiagnoseDTO();
        
        dto.setId(entity.getId());
        LocalDate dateFromEntity = entity.getDiagDate().toLocalDate();
        dto.setDiagDate(dateFromEntity);
        dto.setCause(entity.getCause());
        dto.setResult(entity.getResult());
        dto.setMedicine(entity.getMedicine());
        dto.setVetName(entity.getVetName());
        dto.setPet(PetMapper.toPetDTO(entity.getDiagnosedPet()));

        return dto;
    }
    
    public static List<HealthDiagnoseDTO> toDiagnoseDTOList(List<HealthDiagnoseEntity> entities){
        List<HealthDiagnoseDTO> dtos = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            HealthDiagnoseDTO diagnose = HealthDiagnoseMapper.toDiagnoseDTO(entities.get(i));
            dtos.add(diagnose);
        }
        return dtos;
    }
    
    public static List<HealthDiagnoseEntity> toDiagnoseEntityList(List<HealthDiagnoseDTO> dtos){
        List<HealthDiagnoseEntity> entities = new ArrayList<>();
        for (int i = 0; i < dtos.size(); i++) {
            HealthDiagnoseEntity entity = HealthDiagnoseMapper.toDiagnoseEntity(dtos.get(i));
            entities.add(entity);
        }
        return entities;
    }
    
}
