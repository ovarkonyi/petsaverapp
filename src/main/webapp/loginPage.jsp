<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/login.css"/>
        <style>
            .login-w3-top{
                font-family: "Amatic SC", sans-serif;
            }

            .bejelentkezes{
                background-image: url("resources/media/funnyCats.jpg");
                font-family: Arial;
                min-height: 100vh;
                background-size: cover;
                padding-top: 8%;
                padding-button: 8%;
            }

            .login-card{
                background-color: rgba(42, 47, 51, .8);
                color: #fff;
                font-size: 1.3rem;

            }

            .login-card a{
                color: #eff0f7;
            }

            .login-card-header{
                background-color: rgba(42, 47, 51, .8);
                letter-spacing: 3px;
                font-size: 1.5rem;
                font-weight: 600;
                color: #eff0f7;
            }

            .login-card .form-control{
                background-color:rgba(42, 47, 51, .7);
                border-color: transparent;
                height: 35px;
            }

            .login-card .form-control:focus{
                background-color:rgba(42, 47, 51, .5);
                border-color: #3384AA;
                outline: none;
                box-shadow: none;
                color: #eff0f7;
                font-size: 1.2rem;
            }

            .login-card .form-control::placeholder{
                color: #d6d7de;
                opacity: .8;
                font-size: 1.2rem;
            }

            .btn-login{
                background-color: #298e62;
                letter-spacing: 3px;
                font-weight: 600;
            }

            .btn-login:hover{
                background-color: #2d9b6b;
            }

        </style>
    </head>
    <body class="bejelentkezes">
        <!-- Navbar (sit on top) -->
        <div class="w3-top login-w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Örökbefogadható</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
            </div>
        </div>

        <div class="container">
            <div class="row">

                <div class="col-12 col-lg-6 mx-auto">
                    <div class="card login-card">
                        <div class="card-header login-card-header text-uppercase">
                            Bejelentkezés
                        </div>
                        <form action="j_security_check" method="post" class="cl-effect-14">
                            <div class="card-body">
                                <section class="color-1">
                                    <div class="form-group">
                                        <label for="email">Add meg az e-mail címed</label>
                                        <input type="email" name="j_username" class="form-control" id="j_username" aria-describedby="emailHelp" placeholder="valami@email.hu">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Add meg a jelszavad</label>
                                        <input type="password" name="j_password" class="form-control" id="j_password" placeholder="jelszó">
                                    </div>
<!--                                    <small><a href="#">Elfelejtetted a jelszavadat?</a></small>-->
                                    <div class="card-footer">
<!--                                        <button class="btn btn-login btn-secondary btn-lg btn-block text-uppercase">Bejelentkezés</button>-->
                                        <input type="submit" name="submit" value="Bejelentkezés" class="btn btn-login btn-secondary btn-lg btn-block text-uppercase">
                                    </div>
                                </section>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>