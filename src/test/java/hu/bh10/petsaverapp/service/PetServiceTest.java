package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AnnouncerDAO;
import hu.bh10.petsaverapp.dao.FosterDAO;
import hu.bh10.petsaverapp.dao.PetDAO;
import hu.bh10.petsaverapp.dao.UserDAO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.entity.PetEntity;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PetServiceTest {

    private PetService petService;

    @Mock
    PetDAO petDAO;

    @Mock
    AnnouncerDAO announcerDAO;
    
    @Mock
    UserDAO userDAO;
    
    @Mock
    FosterDAO fosterDAO;
    
    @Before
    public void init(){
        petService = new PetService(petDAO, announcerDAO, userDAO, fosterDAO);
    }

    @Test
    public void testCreatePet() throws Exception {
        PetDTO petDTO = new PetDTO(Long.MIN_VALUE, "Kuty", true, 1, "Fekete", true, "imgage", true);
        petService.createPet(petDTO, Long.MIN_VALUE, "userEmail");
 
        
        Assert.assertEquals("Kuty", petDTO.getNickName());
        Assert.assertEquals(true, petDTO.isCatOrDog());
        Assert.assertEquals(1, petDTO.getAge());
        Assert.assertEquals("Fekete", petDTO.getColor());
        Assert.assertEquals(true, petDTO.isSex());
        Assert.assertEquals("imgage", petDTO.getImage());
        Assert.assertEquals(true, petDTO.isFound());

    }


    @Test
    public void testFindAll() throws Exception {
        List<PetEntity> list = new ArrayList<>();
        PetEntity pet0 = new PetEntity();
        PetEntity pet1 = new PetEntity();
        PetEntity pet2 = new PetEntity();
        
        list.add(pet0);
        list.add(pet1);
        list.add(pet2);
        
        when(petDAO.findAll(PetEntity.class)).thenReturn(list);
        
        List<PetEntity> petList = petService.findAll();
        assertEquals(3, petList.size());
        verify(petDAO, times(1)).findAll(PetEntity.class);
    }

}
